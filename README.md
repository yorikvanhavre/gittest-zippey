This is a test repo to try different strategies to handle FreeCAD files. It uses 
zippey found at

https://bitbucket.org/sippey/zippey/src/master/

Zippey provides two tools to git, clean, that "splits" a zip file into an 
all-in-one text file, ans smudge, that reassembles a zip file from an
all-in-one text file.

How to use/test (on linux):

1. Place some FreeCAD files in a folder
2. Open a terminal in that folder
3. Init a GIT repo: `git init`
4. Register zippey as a clean program: `git config filter.zippey.clean "/path/to/zippey.py e"`
4. Register zippey as a smudge program: `git config filter.zippey.smudge "/path/to/zippey.py d"`
5. Register .FCStd files to be handled by the zippey program: `echo "*.FCStd filter=zippey" >> .gitattributes`
6. Do an initial commit: `git add . && git commit -m "first commit"`
7. Modify the FreeCAD file (ex. addone object)
8. See the differences: `git diff`

You should see a text showing you the differences between the two versions of the file.

See also the gittest-fcinfo repo here for another way using fcinfo.